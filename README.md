These unique wood picture frames are crafted from several different types of wood, including diamond willow wood, as well as driftwood, root wood, and barn wood frames are also available. 

Each of these handcrafted wood photo frames are assembled, sanded and finished by hand.

All of Craig's uniquely shaped picture frames make unique family portrait frames, are a wonderful wedding gift or wood gift for 9th anniversary (The willow wood frames and driftwood frames are very popular), and are a different frame every time you turn them! 
All of the wood used to build these elegantly rustic wood frames is carefully searched for and harvested.

Each frame is crafted from stocks of wood that the crafter himself has deliberately set out to find. Each piece needs to stand out as being peculiar in shape and texture, as well as coloring and grain patterns.

There is then as much care and diligence put into the crafting process. There is much deliberation in the details, without compromising the beauty of any of the natural irregularities of the wood.

When you order custom picture frames online from Craig's Unique Frames, you are going to receive quality wooden picture frames to which the utmost diligence and care have been given, throughout the entirety of the process.

All of Craig's unique handcrafted picture frames come with Artist's Choice glass, hardboard backing and removeable hanger.

Website: https://craigs-unique-frames.com
